##
# File: scraper.py
# Project: pixiv_bookmarks_scraper
# Created Date: 9th August 2017
# Author: Csonka Mihaly
# E-mail: csonka.mihaly@hotmail.com
# -----
##


from enum import Enum
from pixivpy3 import *
from loginwindow import *
from tkinter import filedialog
import os
import errno
_ITEMS_PER_PAGE = 50
# Scrape modes, to be chosen by user
NO_SCRAPE_CHOICE = 0
BOOKMARKED_SCRAPE = 1


# Model
# This class handles the backend stuff and calls tmp_views to communicate with the user
class Scraper(object):
    # Get Pixiv acc data and pass it to the API for auth
    def __login_handler(self):
        login_window = LogInWindow()
        try:
            self.__username = login_window.username
            self.__password = login_window.password
            self.__papi.login(self.__username, self.__password)
            self.__logged_in = True
        except:
            self.__logged_in = False
            # TODO: report to user
            print('Error logging in')

    # Inform user that no scrape optino was selected
    def __no_scrape_choice(self):
        # TODO: report to user
        print('no radiobutton selected.')

    # Handle scraping of bookmarked items
    def __bookmarked_scrape(self):
        # Has user already provided Pixiv acc data?
        if not self.__logged_in:
            self.__login_handler()

        else:
            # Error handle chosen dl location dir
            dir_correct = True
            try:
                os.makedirs(self.__dl_location)
            except OSError as err:
                if err.errno == errno.EEXIST:
                    # TODO: report to user
                    dir_correct = False

            # If everything is ok with the dl path, start processing all items
            if dir_correct:
                page = 1
                json_result = self.__papi.me_favorite_works(
                    page=page, per_page=_ITEMS_PER_PAGE, image_sizes=['large'])
                while len(json_result.response) > 0:
                    for i in range(len(json_result.response)):
                        self.__papi.download(
                            json_result.response[i].work.image_urls.large, path=self.__dl_location)
                    page += 1
                    json_result = self.__papi.me_favorite_works(
                        page=page, per_page=_ITEMS_PER_PAGE, image_sizes=['large'])

    # Switch selecting fn according to user choice
    def start_scrape(self, scrape_type):
        options = {0: self.__no_scrape_choice,
                   1: self.__bookmarked_scrape, }
        options[scrape_type]()

    # Ask user for dl path
    def get_dl_location(self):
        self.__dl_location = filedialog.askdirectory()

    # Init
    def __init__(self):
        self.__logged_in = False
        self.__papi = PixivAPI()
        self.__dl_location = './downloads'
