##
# File: mainwindow.py
# Project: pixiv_bookmarks_scraper
# Created Date: 8th August 2017
# Author: Csonka Mihaly
# E-mail: csonka.mihaly@hotmail.com
# -----
##


from tkinter import *
from scraper import *


# View
# This is the main window that the user will be greeted with
class MainWindow(Tk):
    # Inform user of what to do
    def __create_info_labels(self):
        info_label = Label(
            self, text='Which pictures do you wish to download?')
        info_label.grid(column=0, row=0)

    # Give user choices in regards to what to dl
    def __create_choice_radiobuttons(self):
        self.__scrape_option = IntVar()
        self.__scrape_option.set(NO_SCRAPE_CHOICE)
        self.__radiobuttons = []
        self.__radiobuttons.append(Radiobutton(
            master=self, text='Bookmarked/liked images', variable=self.__scrape_option, value=BOOKMARKED_SCRAPE))
        for r in range(len(self.__radiobuttons)):
            self.__radiobuttons[r].grid(column=0, row=r + 1)

    # Give user choice of where to dl to
    def __create_location_browser(self):
        location_button = Button(
            self, text='Browse', command=self.__scraper.get_dl_location)
        location_button.grid(column=0, row=len(self.__radiobuttons) + 5)

    # This buttons starts the backend stuff
    def __create_start_button(self):
        start_button = Button(self,
                              text='Start', command=lambda: self.__scraper.start_scrape(self.__scrape_option.get()))
        start_button.grid(column=0, row=len(self.__radiobuttons) + 6)

    # Wrapper fn for all of above
    def __create_scrape_choice_form(self):
        self.__create_info_labels()
        self.__create_choice_radiobuttons()
        self.__create_start_button()
        self.__create_location_browser()

    # Init
    def __init__(self, scraper):
        self.__scraper = scraper
        Tk.__init__(self, className='pix-scraper')
        self.__create_scrape_choice_form()
