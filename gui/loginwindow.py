##
# File: loginwindow.py
# Project: pixiv_bookmarks_scraper
# Created Date: 9th August 2017
# Author: Csonka Mihaly
# E-mail: csonka.mihaly@hotmail.com
# -----
##


from tkinter import *


class LogInWindow(object):
    def __save_login_data(self):
        self.username = self.__login_entries[0].get()
        self.password = self.__login_entries[1].get()
        self.__top.destroy()

    def __create_login_labels(self):
        login_labels = []
        login_labels.append(Label(self.__top, text='Username:'))
        login_labels.append(Label(self.__top, text='Password:'))
        for l in range(len(login_labels)):
            login_labels[l].grid(column=0, row=l)

    def __create_login_entries(self):
        self.__login_entries = []
        self.__login_entries.append(Entry(self.__top, justify=LEFT))
        self.__login_entries.append(Entry(self.__top, justify=LEFT, show='*'))
        for e in range(len(self.__login_entries)):
            self.__login_entries[e].grid(column=1, row=e)

    def __create_login_button(self):
        submit_button = Button(self.__top,
                               text='Submit', command=self.__save_login_data)
        submit_button.grid(column=1, row=2)

    def __create_login_form(self):
        self.__create_login_labels()
        self.__create_login_entries()
        self.__create_login_button()

    def __init__(self):
        self.__top = Toplevel()
        self.__create_login_form()
        self.__top.wait_window()
