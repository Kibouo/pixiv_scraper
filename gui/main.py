##
# File: main.py
# Project: pixiv_bookmarks_scraper
# Created Date: 5th August 2017
# Author: Csonka Mihaly
# E-mail: csonka.mihaly@hotmail.com
# -----
##


from mainwindow import *
from scraper import *


scraper = Scraper()
window = MainWindow(scraper)
window.mainloop()
